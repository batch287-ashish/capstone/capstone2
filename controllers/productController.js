const Product = require("../models/Product");
const auth = require("../auth");

module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});
		return newProduct.save().then((product,err) => {
			if(err) return {msg: "Error in creating product.."};
			else return {msg: "Product added successfully."};
		})
	}

	let msg = Promise.resolve("User must be an Admin to access this..");
	return msg.then((val) => {
		return val;
	});
};

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

module.exports.activeProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

module.exports.updateProduct = (reqParams,reqBody,admin) => {
	if(admin){
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,err) => {
			if(err) return {msg: "Error in updating the product."};
			else return {msg: "Product updated successfully"};
		});
	}
	let msg = Promise.resolve("User must be an Admin to access this..");
	return msg.then((val) => {
		return val;
	});
};

module.exports.archiveProduct = (reqParams,admin) => {
	if(admin){
		let archivedProduct = {
			isActive: false
		};
		return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((product,err) => {
			if(err) return {msg: "Error in archiving the product."};
			else return {msg: "Product archived successfully"};
		});
	}
	let msg = Promise.resolve("User must be an Admin to access this..");
	return msg.then((val) => {
		return val;
	});
};

module.exports.activateProduct = (reqParams,admin) => {
	if(admin){
		let activatedProduct = {
			isActive: true
		};
		return Product.findByIdAndUpdate(reqParams.productId,activatedProduct).then((product,err) => {
			if(err) return {msg: "Error in activating the product."};
			else return {msg: "Product activated successfully"};p
		});
	}
	let msg = Promise.resolve("User must be an Admin to access this..");
	return msg.then((val) => {
		return val;
	});
};