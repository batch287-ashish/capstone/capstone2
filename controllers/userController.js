const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({ email: reqBody.email }).then(result => {
		
		if(result.length > 0){
			
			return true

		}else {
			
			return false
		
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,err) => {
		if(err) return {msg: "Error in creating user..."};
		else return true;
	});
};

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) return {msg: "Email doesn't exist."};
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect) return {access: auth.createAccessToken(result)};
			else return {msg: "Password is incorrect."}
		}
	});
};

async function getProductName(productId) {
  try {
    const product = await Product.findOne({ _id: productId }, 'name');
    return product ? product.name : null; 
  } catch (error) {
    return null;
  }
};

async function getTotalAmount(productId,productData) {
	try {
		const product = await Product.findOne({ _id: productId }, 'price');
		return product ? product.price*productData.quantity : null;
	}catch (error){
		return null;
	}
};

module.exports.checkOutProduct = async (data) => {
	if(!data.isAdmin){
		let productData = {
			productId: data.productId,
			productName: await getProductName(data.productId),
			quantity: data.quantity
		};

		let orderData = {
			products: [productData],
			totalAmount: await getTotalAmount(data.productId,productData)
		};

		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.orderedProducts.push(orderData);
			return user.save().then((user,err) => {
				if(err) return false;
				else return true;
			});
		});

		let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.userOrders.push({userId: data.userId});
			return product.save().then((product,err) => {
				if(err) return false;
				else return true;
			});
		});

		if(isUserUpdated&&isProductUpdated) return {msg: "Order placed successfully."};
		else return {msg: "Error in placing order."};
	}
	else{
		let msg = Promise.resolve("Admin can't access this..");
		return msg.then((val) => {
			return val;
		});
	}
};

module.exports.getUserDetails = (data) => {
	return User.findById(data.userId).then((product,err) => {
		if(err) return {msg: "User not found"};
		else return product;
	});
};

module.exports.getAllOrders = (adminCheck) => {
	if(adminCheck){
		return User.find({}).then(products => {
			let sz = products.length;
			let ordersArray = [];
			for(let i=0;i<sz;i++){
				if(products[i].orderedProducts.length>0) ordersArray.push(products[i].orderedProducts);
				else continue;
			}
			return ordersArray;
		});
	}
	else{
		let msg = Promise.resolve("User can't access this..");
		return msg.then((val) => {
			return val;
		});
	}
};

module.exports.getMyOrders = (data) => {
	if(!data.isAdmin){
		return User.findById(data.userId).then(user => {
			let sz = user.orderedProducts.length;
			let ordersArray = [];
			for(let i=0;i<sz;i++){
				ordersArray.push(user.orderedProducts[i]);
			}
			return ordersArray;
		});
	}
	else{
		let msg = Promise.resolve("Admin can't access this..");
		return msg.then((val) => {
			return val;
		});
	}
};

module.exports.setAsAdmin = (reqParams,adminCheck) => {
	let updatedUser = {
		isAdmin: true
	};
	if(adminCheck){
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((newAdmin,err) => {
			if(newAdmin) return "User updated to Admin as per request.";
			else return "Error in updating User to Admin.";
		});
	}
	else{
		let msg = Promise.resolve("User can't access this..");
		return msg.then((val) => {
			return val;
		});
	}
};