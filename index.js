const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoute.js");
const productRoutes = require("./routes/productRoute.js");

const app = express();
const cors=require("cors");
const corsOptions ={
   origin:'*', 
   credentials:true,            //access-control-allow-credentials:true
   optionSuccessStatus:200,
}
app.use(cors(corsOptions));
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.brmhau3.mongodb.net/E-Commerce_API",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open",() => console.log("NOW WE WERE CONNECTED TO THE CLOUD..."));

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use("/users",userRoutes);
app.use("/products",productRoutes);

app.listen(process.env.PORT||4000,()=>{
	console.log(`API is now online on port ${process.env.PORT||4000}`);
});