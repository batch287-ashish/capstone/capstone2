const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});
router.post("/checkOut", auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	userController.checkOutProduct(data).then(resultFromController => res.send(resultFromController));
});
router.get("/userDetails", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getUserDetails({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});
router.get("/orders", auth.verify, (req,res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
});
router.get("/myOrders", auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	userController.getMyOrders(data).then(resultFromController => res.send(resultFromController));
});
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.setAsAdmin(req.params,isAdmin).then(resultFromController => res.send(resultFromController));
});

module.exports = router;